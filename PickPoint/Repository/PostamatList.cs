﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PickPointTest.Repository
{
    public interface IPostomatRepository
    {
        IEnumerable<Postamat> Read(Func<Postamat, bool> predicate = default);
    }
    public class PostamatList : IPostomatRepository
    {
        private readonly List<Postamat> postamats;

        public PostamatList()
        {
            postamats = new List<Postamat>(); //TODO: Add some items 

            postamats.Add(new Postamat("0", "Rostov-on-Don, st. Voroshilovsky 15/44"));
            postamats.Add(new Postamat("1", "Moscow, st. Ordzhenikidze 145 88"));
            postamats.Add(new Postamat("2", "St. Peterspurg, st. Yuzhnaya 5 11", false));
        }

        public IEnumerable<Postamat> Read(Func<Postamat, bool> predicate = null)
        {
            if (predicate is null)
            {
                return postamats.AsEnumerable();
            }
            return postamats.Where(predicate);
        }
    }
}
