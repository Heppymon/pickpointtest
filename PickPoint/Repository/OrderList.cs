﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PickPointTest.Repository
{
    public interface IOrderRepository
    {
        IEnumerable<Order> Read(Func<Order, bool> predicate = default);
        void AddOne(Order item);
        void Remove(Order item);
        void RemoveById(int id);
    }

    public class OrderList : IOrderRepository
    {
        private List<Order> orders;
        public OrderList()
        {
            orders = new List<Order>(); //TODO: Add some items 
        }
        public void AddOne(Order item)
        {
            orders.Add(item);
        }

        public IEnumerable<Order> Read(Func<Order, bool> predicate = null)
        {
            if (predicate is null)
            {
                return orders.AsEnumerable();
            }
            return orders.Where(predicate);
        }

        public void Remove(Order item)
        {
            orders.Remove(item);
        }

        public void RemoveById(int id)
        {
            orders.RemoveAll(x => x.Number == id);
        }
    }
}
