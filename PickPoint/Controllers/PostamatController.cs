﻿using Microsoft.AspNetCore.Mvc;
using PickPointTest.Services;

namespace PickPointTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostamatController : ControllerBase
    {
        private readonly IPostamatService PostamatService;

        public PostamatController(IPostamatService postamatService)
        {
            PostamatService = postamatService;
        }

        [HttpGet("{id}")]
        public Postamat Get(int id)
        {
            try
            {
                return PostamatService.ReadOne(id);
            }
            catch
            {
                Response.StatusCode = 403;
                return null;
            }
        }
    }
}

