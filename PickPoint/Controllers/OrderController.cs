﻿using Microsoft.AspNetCore.Mvc;
using PickPointTest.Services;

namespace PickPointTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService OrderService;
        private IPostamatService PostamatService { get; set; }
        public OrderController(IOrderService orderService, IPostamatService postamatService)
        {
            OrderService = orderService;
            PostamatService = postamatService;
        }

        [HttpGet("{Number}")]
        public Order Get(int Number)
        {
            if (!OrderService.TryReadOne(Number, out Order order))
                Response.StatusCode = 404;

            return order;
        }

        [HttpPost]
        public void Post([FromBody] Order value)
        {
            if (!ValidatePostamat($"{value.Postamat}"))
                return;

            try { OrderService.AddOne(value); }
            catch { Response.StatusCode = 401; }
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Order value)
        {
            if (ValidatePostamat($"{value.Postamat}"))
                return;
            try
            {
                OrderService.RemoveById(value.Number);
                OrderService.AddOne(value);
            }
            catch { Response.StatusCode = 403; }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                OrderService.RemoveById(id);
            }
            catch { Response.StatusCode = 403; }
        }

        public bool ValidatePostamat(string postamatId)
        {
            if (!PostamatService.PostamatExist(postamatId))
            {
                Response.StatusCode = 404;
                return false;
            }
            if (!PostamatService.PostamatActive(postamatId))
            {
                Response.StatusCode = 403;
                return false;
            }
            return true;
        }
    }
}
