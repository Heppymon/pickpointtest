﻿using System.ComponentModel.DataAnnotations;

namespace PickPointTest
{
    public class Postamat
    {
        public Postamat(string number, string address, bool status = true)
        {
            Number = number;
            Address = address;
            Status = status;
        }
        [Required]
        public string Number { get; }
        [Required]
        public string Address { get; set; }
        public bool Status { get; set; }
    }
}
