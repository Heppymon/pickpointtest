﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace PickPointTest.Extentions
{
    public class PhoneFormatAttribute : ValidationAttribute
    {
        public PhoneFormatAttribute() { ErrorMessage = "Incorrect PhoneNumber format"; }

        /// <summary>
        /// Сhecks the validity of the Russian number
        /// </summary>
        /// <param name="value">Russian phone number format +7XXX-XXX-XX-XX where X - is a digit</param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            var regEx = new Regex(@"^\+7\d{3}-\d{3}-\d{2}-\d{2}");
            return regEx.IsMatch(value.ToString()) ? true : false;
        }
    }
}
