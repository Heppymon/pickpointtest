﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PickPointTest.Extentions
{
    public class AvailableStatusAttribute : ValidationAttribute
    {
        public AvailableStatusAttribute() { ErrorMessage = "Status code is unsupported"; }

        public override bool IsValid(object value) => Enum.IsDefined(typeof(Status), value);
    }
}
