using PickPointTest.Extentions;
using System.ComponentModel.DataAnnotations;

namespace PickPointTest
{
    public class Order
    {
        public Order(int number, int status, string[] products, decimal price,
            int postamat, string phoneNumber, string nameOfRecipient)
        {
            Number = number;
            Status = status;
            Products = products;
            Price = price;
            Postamat = postamat;
            PhoneNumber = phoneNumber;
            NameOfRecipient = nameOfRecipient;
        }

        [Required(ErrorMessage = "OrderNumber must be included")]
        public int Number { get; }

        [Required(ErrorMessage = "Status must be included")]
        [AvailableStatus]
        public int Status { get; }

        [MaxLength(10, ErrorMessage = "Maximum number of products is 10")]
        public string[] Products { get; set; }

        public decimal Price { get; set; }

        [Required]
        public int Postamat { get; }

        [PhoneFormat]
        public string PhoneNumber { get; set; }

        public string NameOfRecipient { get; set; }
    }
    public enum Status
    {
        Registred = 1,
        InWarehouse = 2,
        OnCourier = 3,
        OnPostamat = 4,
        Delivered = 5,
        Canceled = 6
    }
}
