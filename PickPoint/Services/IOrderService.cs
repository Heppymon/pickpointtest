﻿using PickPointTest.Repository;
using System.Linq;

namespace PickPointTest.Services
{
    public interface IOrderService
    {
        bool TryReadOne(int Number, out Order order);
        void AddOne(Order value);
        void RemoveById(int id);
    }

    public class OrderService : IOrderService
    {
        private IOrderRepository OrderRepository { get; set; }
        public OrderService(IOrderRepository orderRepository)
        {
            OrderRepository = orderRepository;
        }
        public bool TryReadOne(int Number, out Order order)
        {
            var result = OrderRepository.Read(x => x.Number == Number);
            if (result.Any())
            {
                order = result.First();
                return true;
            }
            order = null;
            return false;
        }

        public void AddOne(Order value)
        {
            OrderRepository.AddOne(value);
        }

        public void RemoveById(int id)
        {
            OrderRepository.RemoveById(id);
        }
    }
}
