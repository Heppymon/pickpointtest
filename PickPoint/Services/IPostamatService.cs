﻿using PickPointTest.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PickPointTest.Services
{
    public interface IPostamatService
    {
        IEnumerable<Postamat> Read(Func<Postamat, bool> predicate);
        Postamat ReadOne(int id);
        bool PostamatExist(string id);
        bool PostamatActive(string id);
    }

    public class PostamatService : IPostamatService
    {
        private IPostomatRepository PostomatRepository { get; set; }
        public PostamatService(IPostomatRepository postomatRepository)
        {
            PostomatRepository = postomatRepository;
        }

        public IEnumerable<Postamat> Read(Func<Postamat, bool> predicate = default)
        {
            return PostomatRepository.Read(predicate);
        }
        public Postamat ReadOne(int id)
        {
            return PostomatRepository.Read(x => x.Number == id.ToString()).First();
        }

        public bool PostamatExist(string id)
        {
            return PostomatRepository.Read().Any(x => x.Number == id) ? true : false;
        }

        public bool PostamatActive(string id)
        {
            return PostomatRepository.Read().First(x => x.Number == id).Status;
        }
    }
}
